import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:ristek_test/endpoints.dart';
import 'package:ristek_test/main_page.dart';

import 'unit_test.dart';

void main() {
  Client? client;
  Client? clientNotFound;
  setUpAll(() {
    client = MockClient((request) async {
      print(request.url.toString());
      if (request.url.toString() == '${Endpoints.searchUser}?q=') {
        return Response(
          jsonEncode(mockData),
          200,
          headers: {'content-type': 'application/json'},
        );
      }
      return Response("", 404);
    });

    clientNotFound = MockClient((request) async {
      return Response("", 404);
    });
  });

  testWidgets('test contain username', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: MainPage(
        client: client,
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 5));

    expect(find.text('octocat'), findsOneWidget);
    expect(find.text('fauziridwan1709'), findsNothing);
  });

  testWidgets('test data not found', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: MainPage(
        client: clientNotFound,
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 5));

    expect(find.text('octocat'), findsNothing);
    expect(find.text('fauziridwan1709'), findsNothing);
  });

  testWidgets('test onchange', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: MainPage(
        client: clientNotFound,
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 5));
    final finder = find.byKey(Key('ContohTextField'));
    expect(finder, findsOneWidget);

    await tester.tap(finder);
    await tester.pumpAndSettle(const Duration(seconds: 45));
    await tester.enterText(finder, 'fauziridwan1709');
  });
}
