// Created by Muhamad Fauzi Ridwan on 23/09/21.

import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:ristek_test/endpoints.dart';
import 'package:ristek_test/github_user.dart';

void main() {
  Client? client;
  setUpAll(() {
    client = MockClient((request) async {
      print(request.url.toString());
      if (request.url.toString() == Endpoints.searchUser) {
        return Response(
          jsonEncode(mockData),
          200,
          headers: {'content-type': 'application/json'},
        );
      }
      return Response("", 404);
    });
  });

  test('UnitTest', () async {
    const mockUsersUrl = Endpoints.searchUser;
    final mockUri = Uri.parse(mockUsersUrl);
    final resp = await client?.get(mockUri);

    expect(resp, isA<Response>());
    expect(200, resp?.statusCode);

    final githubUsers = <GithubUser>[];
    final data = jsonDecode(resp!.body)['items'] as List<dynamic>;
    for (final userJson in data) {
      githubUsers.add(GithubUser.fromJson(userJson));
    }

    expect(githubUsers, isA<List<GithubUser>>());
    expect(githubUsers.length, 2);

    expect(githubUsers.first.toJson(), isA<Map<String, dynamic>>());
  });

  test('UnitTest', () async {
    final data = GithubUser();
    expect(data.url, null);
  });
}

const mockData = {
  'items': [
    {
      "login": "octocat",
      "id": 1,
      "node_id": "MDQ6VXNlcjE=",
      "avatar_url": "https://avatars.githubusercontent.com/u/46?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/octocat",
      "html_url": "https://github.com/octocat",
      "followers_url": "https://api.github.com/users/octocat/followers",
      "following_url":
          "https://api.github.com/users/octocat/following{/other_user}",
      "gists_url": "https://api.github.com/users/octocat/gists{/gist_id}",
      "starred_url":
          "https://api.github.com/users/octocat/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/octocat/subscriptions",
      "organizations_url": "https://api.github.com/users/octocat/orgs",
      "repos_url": "https://api.github.com/users/octocat/repos",
      "events_url": "https://api.github.com/users/octocat/events{/privacy}",
      "received_events_url":
          "https://api.github.com/users/octocat/received_events",
      "type": "User",
      "site_admin": false
    },
    {
      "login": "mojombo",
      "id": 1,
      "node_id": "MDQ6VXNlcjE=",
      "avatar_url":
          "https://secure.gravatar.com/avatar/25c7c18223fb42a4c6ae1c8db6f50f9b?d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png",
      "gravatar_id": "",
      "url": "https://api.github.com/users/mojombo",
      "html_url": "https://github.com/mojombo",
      "followers_url": "https://api.github.com/users/mojombo/followers",
      "subscriptions_url": "https://api.github.com/users/mojombo/subscriptions",
      "organizations_url": "https://api.github.com/users/mojombo/orgs",
      "repos_url": "https://api.github.com/users/mojombo/repos",
      "received_events_url":
          "https://api.github.com/users/mojombo/received_events",
      "type": "User",
      "score": 1,
      "following_url":
          "https://api.github.com/users/mojombo/following{/other_user}",
      "gists_url": "https://api.github.com/users/mojombo/gists{/gist_id}",
      "starred_url":
          "https://api.github.com/users/mojombo/starred{/owner}{/repo}",
      "events_url": "https://api.github.com/users/mojombo/events{/privacy}",
      "site_admin": true
    }
  ]
};
