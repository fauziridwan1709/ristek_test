// Created by Muhamad Fauzi Ridwan on 23/09/21.

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:ristek_test/endpoints.dart';
import 'package:ristek_test/github_user.dart';

class MainPage extends StatefulWidget {
  MainPage({
    Key? key,
    this.client,
  }) : super(key: key);

  final Client? client;

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final usernameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Github user API'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: TextField(
              key: Key('ContohTextField'),
              controller: usernameController,
              decoration: InputDecoration(
                hintText: 'hintText',
                border: OutlineInputBorder(),
              ),
              onChanged: (val) async {
                setState(() {});
              },
            ),
          ),
          Expanded(
            child: FutureBuilder<List<GithubUser>>(
              future: _getGithubUsers.call(),
              builder: (context, _) {
                if (_.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (_.hasData) {
                  final githubUsers = _.data as List<GithubUser>;
                  return ListView.separated(
                    padding: const EdgeInsets.all(20),
                    itemBuilder: (context, index) {
                      final user = githubUsers.elementAt(index);
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            user.login.toString(),
                            style: GoogleFonts.rubik(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            'User id: ${user.id}',
                            style: GoogleFonts.rubik(
                              fontSize: 12,
                            ),
                          ),
                        ],
                      );
                    },
                    separatorBuilder: (_, __) {
                      return Divider();
                    },
                    itemCount: githubUsers.length,
                  );
                } else {
                  return Text('Nothing returned');
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<List<GithubUser>> _getGithubUsers() async {
    final _client = widget.client ?? Client();
    final url =
        Uri.parse('${Endpoints.searchUser}?q=${usernameController.text}');
    Logger().d(url.toString());
    final resp = await _client.get(url);
    final listData = <GithubUser>[];
    final data = jsonDecode(resp.body)['items'] as List<dynamic>;
    Logger().d(data);
    Logger().d(resp.body);
    for (final userJson in data) {
      listData.add(GithubUser.fromJson(userJson));
    }
    return listData;
  }
}
