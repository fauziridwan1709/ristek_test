// Created by Muhamad Fauzi Ridwan on 23/09/21.

class Endpoints {
  static const baseUrl = 'https://api.github.com';
  static const searchUser = '$baseUrl/search/users';
}
